// Having an array with integer numbers rearrange the elements
// to have the positive numbers on the left and the negative numbers on the right.
// Do not use any sorting methods.
public class Exercise_01 {

    public static void main(String[] args) {
        int[] arr = { -2, 1, -3, 4, -1, 2, 1, -5, 4 };
        for (int i = 0; i < arr.length; i++) {
            System.out.print(" " + arr[i]);
        }
        int temp = 0;
        for (int i = 0; i < arr.length; i++) {
            // even
            if (arr[i] < 0) {
                for (int j = i + 1; j < arr.length; j++) {
                    if (arr[j] > 0) {
                        arr[j] = arr[i] + arr[j];
                        arr[i] = arr[j] - arr[i];
                        arr[j] = arr[j] - arr[i];
                        break;
                    }
                }
            }
        }
        System.out.println("");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(" " + arr[i]);
        }
    }
/*    public void arrays(String[] args) {
        int[] arr= {-1,3,4,5,-6,6,8,9,-4};
        ArrayList<Integer> al = new ArrayList<Integer>();
        for (int i=0;i<arr.length;i++) {
            if(arr[i]>0) {
                al.add(arr[i]);
            }
        }
        for (int i=arr.length-1;i>=0;i--) {
            if(arr[i]<0) {
                al.add(arr[i]);
            }
        }
        System.out.println(al);
    }*/
}
