// Write a program that implements the following operations (test the methods in the main() method):
//    1. Add(+)
//    2. Subtract(-)
//    3. Multiply(*)
//    4. Divide(/)
//    5. Sqrt

import java.util.Scanner;

public class Exercise_03 {
    public static double calculateSqrt(int n) {

        double rootVal = 1.00;
        double number = n;
        for (int i = 0; i < n; i++) {

            rootVal = 0.5 * (rootVal + number / rootVal);
        }
        int returnVal = (int) (rootVal);
        rootVal = returnVal;
        System.out.println("The Square root of the number: " + n + ", is = " + rootVal);
        return rootVal;
    }

    public static void main(String[] args) {
        int first;
        int second;
        int add;
        int subtract;
        int multiply;
        float divide;
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter Two Numbers: ");
        first = scanner.nextInt();
        second = scanner.nextInt();

        add = first + second;
        subtract = first - second;
        multiply = first * second;
        divide = (float) first / second;

        System.out.println("Sum = " + add);
        System.out.println("Difference = " + subtract);
        System.out.println("Multiplication = " + multiply);
        System.out.println("Division = " + divide);

        int n;
        Scanner input = new Scanner(System.in);

        System.out.println("Now...\nEnter the Number to find its square root: ");
        n = input.nextInt();
        calculateSqrt(n);

    }
}